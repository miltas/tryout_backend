module.exports = config = {
    defaultPricing: [{
        pricePercent: 0.5,
        notLess: 0.05
    }],
    discountPricing: [{
        client_id: 42,
        price: 0.05
    }],
    highTurnoverDiscount: [{
        amountPerMonth: 1000,
        price: 0.03
    }],
    defaultCurrency: 'EUR'
};
