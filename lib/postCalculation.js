/*jshint strict:false, esversion: 6, node: true */
const separateDate = require('./separateDate');

module.exports = postCalculation;

function postCalculation(transactions, data) {
    const { year, month } = separateDate(data.date);
    let transaction;
    transaction = transactions[data.client_id] || (transactions[data.client_id] = { transactions: [], turnovers: {}});
    transaction.transactions.push(data);
    transaction.turnovers[year] = transaction.turnovers[year] || {};
    transaction.turnovers[year][month] = transaction.turnovers[year][month] || 0;
    transaction.turnovers[year][month] += data.amount;
}
