/*jshint strict:false, esversion: 6, node: true */
const defaultPricing = require('./rules/defaultPricing');
const discountPricing = require('./rules/discountPricing');
const highTurnover = require('./rules/highTurnoverDiscountPricing');

module.exports = calculateAmount;

function calculateAmount(transactions, data) {
    var rules = [];
    rules.push(defaultPricing(data.amount));
    rules.push(discountPricing(data.client_id));
    rules.push(highTurnover(transactions, data));
    rules.sort();

    return rules[0];
}
