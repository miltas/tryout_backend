/*jshint strict:false, esversion: 6, node: true */
module.exports = separateDate;

function separateDate(date) {
    const split = date.split('-');
    return {
        year: split[0],
        month: split[1],
        day: split[2]
    };
}
