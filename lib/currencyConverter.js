/*jshint strict:false, esversion: 6, node: true */

module.exports = (amount, currency, conversionRates) => amount/conversionRates.rates[currency];
