/*jshint strict:false, esversion: 6, node: true */
const config = require('../../config');
module.exports = discountPricing;

function discountPricing(clientId) {
    let result;

    if (config.discountPricing[0].client_id === clientId) {
        result = config.discountPricing[0].price;
    }

    return result;
}
