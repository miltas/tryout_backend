/*jshint strict:false, esversion: 6, node: true */
const config = require('../../config');
const separateDate = require('../separateDate');

module.exports = highTurnoverDiscount;

function highTurnoverDiscount(transactions, request) {
    let result;
    const clientId = request.client_id;
    const { year, month } = separateDate(request.date);
    const turnovers = transactions[clientId] && transactions[clientId].turnovers;
    const {amountPerMonth, price} = config.highTurnoverDiscount[0];

    if (turnovers && turnovers[year][month] > amountPerMonth) {
        result = price;
    }

    return result;
}
