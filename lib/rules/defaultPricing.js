/*jshint strict:false, esversion: 6, node: true */
const config = require('../../config');
module.exports = defaultPricing;

function defaultPricing(amount) {
    let result;
    const pricePercent = config.defaultPricing[0].pricePercent;
    const notLess = config.defaultPricing[0].notLess;
    const calc = amount * (pricePercent/100);

    result = calc >= notLess ? calc : notLess;

    return parseFloat(result.toFixed(2));
}
