/*jshint strict:false, esversion: 6, node: true */
exports.date = validateDate;
exports.amount = validateAmount;
exports.currency = validateCurrency;
exports.clientId = validateClientId;
exports.contentType = validateContentType;
exports.body = validateBody;

function validateDate (req, res, next) {
    const date = new Date(req.body.date);
    let today, error;

    if (date != 'Invalid Date') {
        today = Date.now();
        error = date > today ? 'Commission is not possible for the future' : error;
    } else {
        error = 'Incorrect date type';
    }

    if (error) {
        errorMessage(res, error, 400);
    } else {
        next();
    }
}

function validateAmount(req, res, next) {
    let amount = req.body.amount;
    if (/^\d+\.\d+$/.test(amount)){
        next();
    } else {
        errorMessage(res, 'Amount must be positive float', 400);
    }
}

function validateCurrency (req, res, next, rates) {
    if (rates[req.body.currency]) {
        next();
    } else {
        errorMessage(res, `Currency ${req.body.currency} does not exist, try uppercase`, 400);
    }
}

function validateClientId (req, res, next) {
    let clientId = req.body.client_id;
    if (typeof clientId === 'number' && clientId >= 0 &&  Number.isInteger(clientId)) {
        next();
    } else {
        errorMessage(res, 'client_id must be integer', 400);
    }
}

function validateContentType (req, res, next) {
    if (req.is('application/json')) {
        next();
    } else {
        errorMessage(res, 'Request failed: Invalid POST body data. Must be JSON format', 400);
    }
}

function validateBody (req, res, next) {
    const keys = [ 'date', 'amount', 'currency', 'client_id' ];
    let notCorrect = false;
    for (let key in req.body) {
        notCorrect = keys.indexOf(key) >= 0 ? false : notCorrect;
    }
    if (notCorrect) {
        errorMessage(res, 'Bad JSON', 400);
    } else {
        next();
    }
}

function errorMessage (res, message, code) {
    res.setHeader('Content-Type', 'application/json');
    res.status(code).json({error: message});
}