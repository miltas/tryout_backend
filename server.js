/*jshint esversion: 6 */
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const validate = require('./lib/validate');
const defaultCurrency = require('./config').defaultCurrency;
const calculateAmount = require('./lib/calculateAmount');
const postCalculation = require('./lib/postCalculation');
const convertAmount = require('./lib/currencyConverter');
const app = express();

const port = 3000;
const jsonParser = bodyParser.json();
const transactions = {};
let conversionRates = {};
let server;

http.get('http://api.exchangerate.host/2021-01-01', (res) => {
    let rawData = '';
    res.on('data', (chunk) => { rawData += chunk; });
    res.on('end', () => {
        try {
            conversionRates = JSON.parse(rawData);
        } catch (e) {
            console.error(e.message);
            server && server.close();
        }
    });
}).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
    server && server.close();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(validate.contentType);
app.use(jsonParser);

app.post('/transaction', 
    validate.body,
    validate.date,
    validate.amount,
    validate.clientId,
    (req, res, next) => {
        validate.currency(req, res, next, conversionRates.rates);
    },
    (req, res) => {
        let amount;
        const data = req.body;
        if (data.currency !== defaultCurrency) {
            data.originalAmount = parseFloat(data.amount);
            data.originalCurrency = data.currency;
            data.amount = parseFloat(convertAmount(data.originalAmount, data.originalCurrency, conversionRates));
            data.currency = defaultCurrency;
        } else {
            data.amount = parseFloat(data.amount);
        }

        amount = calculateAmount(transactions, data);
        postCalculation(transactions, data);

        res.setHeader('Content-Type', 'application/json');
        res.status(200).json({
            amount: amount || 'notYet',
            currency: defaultCurrency
        });
    }
);

server = app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});

exports.app = app;
exports.server = server;
