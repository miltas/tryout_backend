/*jshint strict:false, esversion: 6, node: true */
const request = require('supertest');

describe("server", () => {
    let express, server, app;
    beforeEach(() => { 
        express = require('../server');
        app = express.app;
        server = express.server;
    });

    it("should not accept anything than json", (done) => {
        const instance = request(app);
        setTimeout(() => {
            instance.post('/transaction')
            .send('asdasdas')
            .set('Content-Type', 'text/html')
            .expect('Content-Type', /json/)
            .expect(400, {
                "error": "Request failed: Invalid POST body data. Must be JSON format"
            }, done);
            server.close();
        }, 100);
    });
    it('should respond with discount pricing', (done) => {
        const instance = request(app);
        setTimeout(() => {
            instance.post('/transaction')
            .send({
                "date": "2022-02-03",
                "amount": "300.00",
                "currency": "EUR",
                "client_id": 42
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                amount: 0.05,
                currency: 'EUR'
            }, done);
            server.close();
        }, 100);
    });
    it('should respond with high turnover discount pricing', (done) => {
        const instance = request(app);
        setTimeout(() => {
            instance.post('/transaction')
            .send({
                "date": "2022-02-03",
                "amount": "900.00",
                "currency": "EUR",
                "client_id": 41
            })
            .end(() => {
                instance.post('/transaction')
                .send({
                    "date": "2022-02-03",
                    "amount": "200.00",
                    "currency": "EUR",
                    "client_id": 41
                })
                .end(() => {
                    instance.post('/transaction')
                    .send({
                        "date": "2022-02-03",
                        "amount": "200.00",
                        "currency": "EUR",
                        "client_id": 41
                    })
                    .end(() => {
                        instance.post('/transaction')
                        .send({
                            "date": "2022-02-03",
                            "amount": "200.00",
                            "currency": "EUR",
                            "client_id": 41
                        })    
                        .expect('Content-Type', /json/)
                        .expect(200, {
                            amount: 0.03,
                            currency: 'EUR'
                        }, done);
                    })
                })
            })
            server.close();
        }, 100);
    });
    it('should respond with default pricing', (done) => {
        const instance = request(app);
        setTimeout(() => {
            instance.post('/transaction')
            .send({
                "date": "2022-02-03",
                "amount": "900.00",
                "currency": "EUR",
                "client_id": 40
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                amount: 4.5,
                currency: 'EUR'
            }, done);
            server.close();
        }, 100);
    });
});
