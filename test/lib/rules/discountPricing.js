/*jshint strict:false, esversion: 6, node: true */

const assert = require('assert');
const sandboxed = require('sandboxed-module');
const config = require('../../../config');

describe("discountPricing", () => {
    let discountPricing;
    beforeEach(() => {
        discountPricing = sandboxed.load('../../../lib/rules/discountPricing').exports;
    });
    it("should return function", () => {
        assert.strictEqual(typeof discountPricing, 'function');
    });
    it("should return amount based on config if clientId matches", () => {
        assert.strictEqual(discountPricing(config.discountPricing[0].client_id), config.discountPricing[0].price);
    });
    it("should return undefined if client_id does not match config", () => {
        assert.ok(!discountPricing(1));
    });
});
