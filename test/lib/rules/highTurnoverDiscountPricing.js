/*jshint strict:false, esversion: 6, node: true */

const assert = require('assert');
const sandboxed = require('sandboxed-module');
const config = require('../../../config');

describe("highTurnoverDiscountPricing", () => {
    let highTurnover, stub;
    beforeEach(() => {
        highTurnover = sandboxed.load('../../../lib/rules/highTurnoverDiscountPricing').exports;
        stub = {
            transactions: {},
            data: {
                "date": "2022-02-03",
                "amount": "300.00",
                "currency": "EUR",
                "client_id": 10
            }    
        };
    });
    it("should return function", () => {
        assert.strictEqual(typeof highTurnover, 'function');
    });
    it("should return undefined if no transactions are made in the past for particular month", () => {
        assert.ok(!highTurnover(stub.transactions, stub.data));
    });
    describe("transactions", () => {
        beforeEach(() => {
            stub.transactions = {
                "10": {
                    transactions: {},
                    turnovers: {
                        "2022": {
                            "02": 900
                        }
                    }
                }
            };
        });
        it("should return undefined if turnover is smaller than described in config", () => {
            stub.transactions['10'].turnovers['2022']['02'] = config.highTurnoverDiscount[0].amountPerMonth - 200;
            assert.ok(!highTurnover(stub.transactions, stub.data));
        });
        it("should return described amount if turnover is bigger than described in config", () => {
            stub.transactions['10'].turnovers['2022']['02'] = config.highTurnoverDiscount[0].amountPerMonth + 200;
            assert.strictEqual(highTurnover(stub.transactions, stub.data), config.highTurnoverDiscount[0].price);
        });
        it("should return undefined if turnover exist for the same month but different year", () => {
            stub.transactions['10'].turnovers['2021'] = {
                "02": config.highTurnoverDiscount[0].amountPerMonth + 200
            };
            assert.ok(!highTurnover(stub.transactions, stub.data));
        });
        it("should return undefined if turnover exist for different client", () => {
            stub.transactions['10'].turnovers['2022']['02'] = config.highTurnoverDiscount[0].amountPerMonth + 200;
            stub.data.client_id = 2;
            assert.ok(!highTurnover(stub.transactions, stub.data));
        });
    });
});
