/*jshint strict:false, esversion: 6, node: true */

const assert = require('assert');
const sandboxed = require('sandboxed-module');
const config = require('../../../config');

describe("defaultPricing", () => {
    let defaultPricing;
    beforeEach(() => {
        defaultPricing = sandboxed.load('../../../lib/rules/defaultPricing').exports;
    });
    it("should return function", () => {
        assert.strictEqual(typeof defaultPricing, 'function');
    });
    describe("calculation", () => {
        it("should return minimum amount based on config", () => {
            assert.strictEqual(defaultPricing(1), config.defaultPricing[0].notLess);
        });
        it("should return percentage based on config", () => {
            assert.strictEqual(defaultPricing(500.00), calculatePercentage(500));
            assert.strictEqual(defaultPricing(499.00), calculatePercentage(499));
            assert.strictEqual(defaultPricing(1200.01), calculatePercentage(1200.01));
        });
    });

    function calculatePercentage (amount) {
        let percentage = config.defaultPricing[0].pricePercent;
        return parseFloat((amount*percentage/100).toFixed(2));
    }
});
