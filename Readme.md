# Home task assignment

## Prerequisite
Install Node.js. https://nodejs.org/en/download/

Install dependencies `npm i`

## Run
`npm run start`

## Run in development mode
`npm run debug`

## Unit and Integration tests
`npm run tests`

## Time spent
Spent around 7 hours.
